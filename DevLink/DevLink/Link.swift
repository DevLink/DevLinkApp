import Foundation


struct Link: Equatable {
    let host: String
    let user: String
    let repo: String
    let ref: String  // tag, commit or branch
    let file: String? // path and file name, nil for repo root links
    let line: String? // line number if available
}
