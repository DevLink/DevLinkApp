import Foundation
import Carbon
import os.log


final class XcodeHandler: AppHandlerLinkSource, AppHandlerLinkOpener {
    
    var bundleIdentifier: String = "com.apple.dt.Xcode"
    
    init() {
        // Required for dynamic creation of instances basing on protocol
    }
    
    // MARK: - AppHandlerLinkSource implementation
    
    func extractLink() -> Result<Link, AppHandlerError> {
        let scriptSource = """
        tell application "Xcode"
            set CurrentActiveDocument to document 1 whose name ends with (word -1 of (get name of window 1))
            set documentPath to path of CurrentActiveDocument
        end tell
        """
        
        let script = NSAppleScript(source: scriptSource)!
        var error: NSDictionary?
        let event = script.executeAndReturnError(&error)
        
        if let error = error {
            print(error)
            os_log("Can't extract link from Xcode. Apple Script execution failed.", type: .error)
        } else {
            if let filePath = event.stringValue {
                let link = GitWorktreeCommands().getHostingLink(for: filePath)
                return link
            }
        }
        
        return .failure(AppHandlerError.noLinkReturnedByApp)
    }
    
    // MARK: - AppHandlerLinkOpener implementation
    
    func openLink(link: Link) -> Result<Void, AppHandlerError> {
        guard
            let filePath = link.file
        else {
            os_log("No file name in the link.", type: .error)
            return .failure(.noFilePathInLink)
        }
        
        guard
            let volumeName = getVolumeName(),
            let projectsDir = UserDefaults.standard.string(forKey: projectsDirKey)
        else {
            os_log("No projects dir set", type: .error)
            return .failure(.projectsDirNotSet)
        }
        
        let prefix = volumeName + toScriptPath(unixPath: projectsDir)
        let repo = link.repo
        let workspacePath = toScriptPath(unixPath: filePath)
        
        let convertedFilePath = "\(prefix):\(repo):\(workspacePath)"
        
        let scriptSource = """
        tell application "Xcode"
            activate
            open file "\(convertedFilePath)"
        end tell
        """
        
        let script = NSAppleScript(source: scriptSource)!
        var error: NSDictionary?
        _ = script.executeAndReturnError(&error)
        
        if let error = error,
           let errorMessage = error[NSAppleScript.errorMessage] as? String {
                
                os_log("Xcode handler `openLink` script execution failed.", type: .error)
                return .failure(.appleScriptExecutionError(message: errorMessage))
        }
        
        return .success(())
    }
}

// MARK: - Helper functions

func toUnixPath(scriptPath: String) -> String {
    return scriptPath.replacingOccurrences(of: ":", with: "/")
}

func toScriptPath(unixPath: String) -> String {
    return unixPath.replacingOccurrences(of: "/", with: ":")
}

func getVolumeName() -> String? {
    if let session = DASessionCreate(kCFAllocatorDefault) {
        let mountedVolumeURLs = FileManager.default.mountedVolumeURLs(includingResourceValuesForKeys: nil)!
        
        let rootVolume = mountedVolumeURLs.first{ $0.path == "/"}
        
        if let disk = DADiskCreateFromVolumePath(kCFAllocatorDefault, session, rootVolume! as CFURL),
           let description = DADiskCopyDescription(disk),
           let dictionary = description as? [String: AnyObject],
           let volumeName = dictionary["DAVolumeName"] {
            
                return volumeName as? String
        }
    }
    os_log("Can't get volume name.", type: .error)
    return nil
}
