import Cocoa
import os.log


let projectsDirKey = "ProjectsDir"
let bitbucketServerHostKey = "BitbucketServerHost"
let xcodeFileAssociationsKey = "XcodeFileAssociations"
let ideaFileAssociationsKey = "IDEAFileAssociations"
let isSafariAuthorizedKey = "safariAuthorization"
let isChromeAuthorizedKey = "chromeAuthorization"
let isXcodeAuthorizedKey = "xcodeAuthorization"


/// Settings
class ViewController: NSViewController {
    
    // Projects tab
    @IBOutlet weak var projectsDir: NSTextField!
    
    // Apps tab
    @IBOutlet weak var xcodeFileExtensions: NSTextFieldCell!
    @IBOutlet weak var ideaFileExtensions: NSTextFieldCell!
    
    @IBOutlet weak var safariPermissionsLabel: NSTextFieldCell!
    @IBOutlet weak var authorizeSafariButton: NSButton!
    @IBOutlet weak var chromePermissionsLabel: NSTextFieldCell!
    @IBOutlet weak var authorizeChromeButton: NSButton!
    @IBOutlet weak var xcodePermissionsLabel: NSTextFieldCell!
    @IBOutlet weak var authorizeXcodeButton: NSButton!
    
    // Hostings tab
    @IBOutlet weak var bitbucketServerHost: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let projects = UserDefaults.standard.string(forKey: projectsDirKey) {
            projectsDir.stringValue = projects
        }
        
        updatePermissionLabelsAndButtons();
    }
    
    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    @IBAction func selectProjectsDir(_ sender: Any) {
        let dialog = NSOpenPanel();
        
        dialog.title = "Choose projects directory"
        dialog.showsResizeIndicator = true
        dialog.showsHiddenFiles = false
        dialog.allowsMultipleSelection = false
        dialog.canChooseDirectories = true
        
        if (dialog.runModal() ==  NSApplication.ModalResponse.OK) {
            if let result = dialog.url {
                UserDefaults.standard.setValue(result.path, forKey: projectsDirKey)
            } else {
                os_log("Unknown error during projects dir selection", type: .error)
            }
            
        } else {
            print("User canceled dialog")
            return
        }
    }
    
    @IBAction func safariBtnClicked(_ sender: Any) {
        let source = """
            tell application "Safari"
                set theURL to URL of current tab of window 1
            end tell
        """
        
        let script = NSAppleScript(source: source)!
        var error: NSDictionary?
        _ = script.executeAndReturnError(&error)
        
        UserDefaults.standard.set(error == nil, forKey: isSafariAuthorizedKey)
        updatePermissionLabelsAndButtons()
    }
    
    @IBAction func chromeBtnClicked(_ sender: Any) {
        let source = """
            tell application "Google Chrome"
                set theURL to URL of active tab of front window
            end tell
        """
        
        let script = NSAppleScript(source: source)!
        var error: NSDictionary?
        _ = script.executeAndReturnError(&error)
        
        UserDefaults.standard.set(error == nil, forKey: isChromeAuthorizedKey)
        updatePermissionLabelsAndButtons()
    }
    
    @IBAction func xcodeBtnClicked(_ sender: Any) {
        let source = """
            tell application "Xcode"
                set wsp to active workspace document
                return file of wsp
            end tell
        """
        
        let script = NSAppleScript(source: source)!
        var error: NSDictionary?
        _ = script.executeAndReturnError(&error)
        
        UserDefaults.standard.set(error == nil, forKey: isXcodeAuthorizedKey)
        updatePermissionLabelsAndButtons()
    }
    
    private func updatePermissionLabelsAndButtons() {
        
        let isSafariAuthorized = UserDefaults.standard.bool(forKey: isSafariAuthorizedKey)
        authorizeSafariButton.isHidden = isSafariAuthorized
        safariPermissionsLabel.title =
            isSafariAuthorized ? "Safari is authorized" : "DevLink is not authorized to access Safari"
        
        let isChromeAuthorized = UserDefaults.standard.bool(forKey: isChromeAuthorizedKey)
        authorizeChromeButton.isHidden = isChromeAuthorized
        chromePermissionsLabel.title =
            isChromeAuthorized ? "Google Chrome is authorized" : "DevLink is not authorized to access Chrome"
        
        let isXcodeAuthorized = UserDefaults.standard.bool(forKey: isXcodeAuthorizedKey)
        authorizeXcodeButton.isHidden = isXcodeAuthorized
        xcodePermissionsLabel.title =
            isXcodeAuthorized ? "Xcode is authorized" : "DevLink is not authorized to access Xcode"
    }
}

