# DevLink App for macOS.

Switch between viewing code in browser and editing it in your favourite IDE in a blink!

## How it works?

Press Control+Option+Command+L while viewing a file on Bitbucket or GitHub and if you have a working copy of this repository locally DevLink will open it in Xcode or Andoid Studio. By default all Swift and Objective-C files opened in Xcode and all Java and Kotlin files in Android Studio. You can change list of extensions in the settings. Also, if you are working on a project you cloned from Bitbucket or GitHub in Xcode you can press Control+Option+Command+L to open this on git hosting from which you cloned it. For all unknow file types DevLink will try to open Sublime Text if you have it installed.


## Installation

The easiest way to install the app is to download the latest version from [releases page](https://codeberg.org/DevLink/DevLinkApp/releases). After unzipping drag and drop `DevLink.app` to Applications folder. On the first start macOS will show worning that this applications is not signed and can't be opened. Ctrl+click on the DevLink again and choose "Open" or go to System Preferences -> Security & Private and on the General tab click "Open" button next to the system warning about DevLink.

You can also download source code and build it using Xcode 12.3

⚠️ Setup projects search path in the app settings ⚠️

Otherwise DevLink woun't be able to find local copies of git repositories. 


## Contributing

At the moment source code is a bit messy. Initially it was intended for my personal use only. It was a very simple tool for switching between Xcode and Safari or Google and supported only one git hosting. I plan to clean it up soon, so you have are thinking about adding a feature I recommend openning an issue instead with feature request instead of clonning this repo.