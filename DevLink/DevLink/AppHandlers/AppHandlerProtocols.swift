/// An App Handler can implement both protocols if it has both capabilities

protocol AppHandlerLinkSource: class {
    init()
    var bundleIdentifier: String { get }
    func extractLink() -> Result<Link, AppHandlerError>
}

protocol AppHandlerLinkOpener: class {
    init()
    var bundleIdentifier: String { get }
    func openLink(link: Link) -> Result<Void, AppHandlerError>
}

enum AppHandlerError: Error, Equatable {
    case noPermissionOrAppClosed(message: String)
    case projectsDirNotSet
    case appleScriptExecutionError(message: String)
    case noLinkReturnedByApp
    case cantExecuteOpenerApp(message: String)
    case cantParseFilePath
    case noFilePathInLink
    case notAGitRepo
    case noRemoteSetForRepo
    case other(message: String)
}

