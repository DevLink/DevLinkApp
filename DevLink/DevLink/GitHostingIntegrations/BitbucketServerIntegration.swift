import Foundation
import os.log


// Path formats:
// - /projects/{user}/repos/{repo}/browse/{file}
// - /projects/{user}/repos/{repo}/pull-requests/{PR_id}/diff#{file}
// - /projects/{user}/repos/{repo_name}/pull-requests/{PR_id}/diff?commentId={comment_id}#{file}?f={some_other_id}
// - /projects/{user}/repos/{repo_name}/commits/{commit_hash}#{file}
final class BitbucketServerIntegration: GitHostingIntegrationProtocol {
    
    var host: String
    
    init() {
        // Init is required for dynamic creation of instances basing on protocol.
        
        host = UserDefaults.standard.string(forKey: bitbucketServerHostKey) ?? ""
    }
    
    func url2Link(url: URL) -> Link? {
        let link: Link?
        if isPROrCommitURL(url) {
            link = parsePRURL(url)
        } else {
            link = parseRegularURL(url)
        }
                
        return link
    }
    
    func link2URL(link: Link) -> URL? {
        var urlComponents: [String] = [link.host, "projects", link.user, "repos", link.repo, "browse"]
        
        if let file = link.file,
           let encodedFileName = file.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            urlComponents.append(encodedFileName)
        }
        
        let urlString = "https://" + urlComponents.joined(separator: "/")
        return URL(string: urlString)
    }
    
    // MARK: -  Helpers
    
    private func isPROrCommitURL(_ url: URL) -> Bool {
        url.absoluteString.contains("pull-requests") || url.absoluteString.contains("commits")
    }
    
    private func parsePRURL(_ url: URL) -> Link? {
        guard let hostName = url.host else {
            os_log("Can't find host in URL", type: .error)
            return nil
        }
        
        let pathComponents = url.pathComponents
        guard let maybeFile = url.fragment else {
            os_log("Bitbucket Server PR URL should have fragment with file name.", type: .error)
            return nil
        }
        
        let file: String
        if let questionMarkIndex = maybeFile.firstIndex(of: "?") {
            file = String(maybeFile[maybeFile.startIndex..<questionMarkIndex])
        } else {
            file = maybeFile
        }
        
        guard
            let fileName = file.removingPercentEncoding
        else {
            os_log("Can't extract file name from URL", type: .error)
            return nil
        }
        
        let link = Link(host: hostName,
                        user: String(pathComponents[2]),
                        repo: String(pathComponents[4]),
                        ref:  "",
                        file: String(fileName),
                        line: nil)
        return link
    }
    
    private func parseRegularURL(_ url: URL) -> Link? {
        guard let hostName = url.host else {
            os_log("Can't find host in URL", type: .error)
            return nil
        }
        
        let pathComponents = url.pathComponents
        let fileName = pathComponents.count >= 7 ? pathComponents.dropFirst(6).joined(separator: "/") : nil
        let encodedFileName = fileName?.removingPercentEncoding
        
        let link = Link(host: hostName,
                        user: String(pathComponents[2]),
                        repo: String(pathComponents[4]),
                        ref: "",
                        file: encodedFileName,
                        line: nil)
        return link
    }
    
}

