import Foundation


/// Path format: {user}/{repo_name}/blob/{ref}/{file}}

final class GithubIntegration: GitHostingIntegrationProtocol {
    
    var host: String { "github.com" }
    
    init() {
        // Required for dynamic creation of instances basing on protocol
    }
    
    func url2Link(url: URL) -> Link? {
        let pathComponents = url.pathComponents
        
        let fileName = pathComponents.count >= 6 ? pathComponents.dropFirst(5).joined(separator: "/") : nil
        let encodedFileName = fileName?.removingPercentEncoding
        
        let link: Link = Link(host: host,
                              user: String(pathComponents[1]),
                              repo: String(pathComponents[2]),
                              ref: String(pathComponents[4]),
                              file: encodedFileName,
                              line: nil)
        
        return link
    }
    
    func link2URL(link: Link) -> URL? {
        var urlComponents: [String] = [link.host, link.user, link.repo, "blob", link.ref]
        
        if let file = link.file,
           let encodedFileName = file.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                urlComponents.append(encodedFileName)
        }
        
        let urlString = "https://" + urlComponents.joined(separator: "/")
        return URL(string: urlString)
    }
    
}
