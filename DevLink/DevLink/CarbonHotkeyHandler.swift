import Carbon
import Cocoa
import os.log


class CarbonHotkeyHandler {
    
    static func register() {
        var hotKeyRef: EventHotKeyRef?
        let modifierFlags: UInt32 =
            getCarbonFlagsFromCocoaFlags(cocoaFlags: [NSEvent.ModifierFlags.command,
                                                      NSEvent.ModifierFlags.option,
                                                      NSEvent.ModifierFlags.control])
        
        let keyCode = kVK_ANSI_L
        var gMyHotKeyID = EventHotKeyID()
        gMyHotKeyID.id = UInt32(keyCode)
        gMyHotKeyID.signature = OSType("swat".fourCharCodeValue)
        
        var eventType = EventTypeSpec()
        eventType.eventClass = OSType(kEventClassKeyboard)
        eventType.eventKind = OSType(kEventHotKeyReleased)
        
        
        // Install handler.
        InstallEventHandler(GetApplicationEventTarget(), { (nextHanlder, theEvent, userData) -> OSStatus in
            // Since this is a Carbon API and this closure will be converted to
            // a C function pointer, no context capturing is allowed inside this closure.
            CarbonHotkeyHandler.extractAndOpenLink()
            return noErr
        }, 1, &eventType, nil, nil)
        
        // Register hotkey.
        let status = RegisterEventHotKey(UInt32(keyCode),
                                         modifierFlags,
                                         gMyHotKeyID,
                                         GetApplicationEventTarget(),
                                         0,
                                         &hotKeyRef)
        assert(status == noErr)
    }
    
    private static func extractAndOpenLink() {
        // In the future if this App will be showing some kind of UI
        // this step should be done before showing DevLinks window.
        let frontApp = NSWorkspace.shared.frontmostApplication
        
        let appsRegistry = AppHandlersRegitstry()
        
        if
            let appBundleIdentifier = frontApp?.bundleIdentifier,
            let linkHandler = appsRegistry.sourceHandlers[appBundleIdentifier] {
                
                let result = linkHandler.extractLink()
                switch result {
                    case .success(let link):
                        if appBundleIdentifier == "com.apple.dt.Xcode" {
                            // It's a link from Xcode. Open it in the default browser.
                            
                            let hostingRegistry = GitHostingsRegistry()
                            let hostingIntegration = hostingRegistry.hostingIntegrations[link.host]
                            if let linkURL = hostingIntegration?.link2URL(link: link) {
                                NSWorkspace.shared.open(linkURL)
                            } else {
                                os_log("Can't convert link to URL.", type: .error)
                            }
                        } else {
                            // It's a link from browser.
                            
                            openInApp(link, appsRegistry)
                        }
                    case .failure(let handlerError):
                        print(handlerError)
                        os_log("Link extractor returned error.", type: .error)
                }
        }
    }
    
    static func openInApp(_ link: Link, _ appsRegistry: AppHandlersRegitstry) {
        guard
            let fileName = link.file
        else {
            os_log("No file name in the link", type: .error)
            return
        }
        
        let fileExtension = getFileExtension(fileName: fileName)
        
        if xcodeFileAssociations.contains(fileExtension) {
            // Open in Xcode
            let openHandler = appsRegistry.openerHandlers["com.apple.dt.Xcode"]
            _ = openHandler?.openLink(link: link)
        } else if ideaFileAssociations.contains(fileExtension) {
            // Open in IntelliJ IDEA
            let openHandler = appsRegistry.openerHandlers["com.jetbrains.intellij.ce"]
            _ = openHandler?.openLink(link: link)
        } else {
            // Open in Sublime Text
            let openHandler = appsRegistry.openerHandlers["com.sublimetext.3"]
            _ = openHandler?.openLink(link: link)
        }
    }
    
    private static func getFileExtension(fileName: String) -> String {
        guard
            let fileExtensionStartIndext = fileName.lastIndex(of: ".")
        else {
            // File without extension
            return ""
        }
        
        let fileExtension = fileName[fileExtensionStartIndext...]
        return String(fileExtension)
    }
    
    private static var xcodeFileAssociations: [String] {
        let defaultExtensions = [".swift", ".m", ".mm", ".c", ".cc", ".cpp", ".h", ".hpp", ".plist",
                                 ".xib", ".nib", ".xcodeproj", ".storyboard", ".xcworkspace"]
        
        if let userDefinedExtensionsString = UserDefaults.standard.string(forKey: xcodeFileAssociationsKey) {
            let userDefinedExtensions = userDefinedExtensionsString.split(separator: " ").map{ String($0) }
            return userDefinedExtensions
        } else {
            return defaultExtensions
        }
    }
    
    private static var ideaFileAssociations: [String] {
        let defaultExtensions = [".kt", ".java", ".xml"]
        
        if let userDefinedExtensionsString = UserDefaults.standard.string(forKey: ideaFileAssociationsKey) {
            let userDefinedExtensions = userDefinedExtensionsString.split(separator: " ").map{ String($0) }
            return userDefinedExtensions
        } else {
            return defaultExtensions
        }
    }
    
    private static func getCarbonFlagsFromCocoaFlags(cocoaFlags: NSEvent.ModifierFlags) -> UInt32 {
        let flags = cocoaFlags.rawValue
        var newFlags: Int = 0
        
        if ((flags & NSEvent.ModifierFlags.control.rawValue) > 0) {
            newFlags |= controlKey
        }
        
        if ((flags & NSEvent.ModifierFlags.command.rawValue) > 0) {
            newFlags |= cmdKey
        }
        
        if ((flags & NSEvent.ModifierFlags.shift.rawValue) > 0) {
            newFlags |= shiftKey;
        }
        
        if ((flags & NSEvent.ModifierFlags.option.rawValue) > 0) {
            newFlags |= optionKey
        }
        
        if ((flags & NSEvent.ModifierFlags.capsLock.rawValue) > 0) {
            newFlags |= alphaLock
        }
        
        return UInt32(newFlags);
    }
}

// MARK: -

fileprivate extension String {
    /// This converts string to UInt as a fourCharCode
    var fourCharCodeValue: Int {
        var result: Int = 0
        if let data = self.data(using: String.Encoding.macOSRoman) {
            data.withUnsafeBytes({ (rawBytes) in
                let bytes = rawBytes.bindMemory(to: UInt8.self)
                for i in 0 ..< data.count {
                    result = result << 8 + Int(bytes[i])
                }
            })
        }
        return result
    }
}
