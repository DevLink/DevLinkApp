import AppKit
import Foundation
import os.log


final class SublimeHandler: AppHandlerLinkOpener {
    
    var bundleIdentifier: String { "com.sublimetext.3" }
    
    private var runner: ProcessRunner
    
    // Required for dynamic creation of instances basing on protocol
    convenience init() {
        self.init(runner: DefaultProcessRunner())
    }
    
    init(runner: ProcessRunner = DefaultProcessRunner()) {
        self.runner = runner
    }
    
    // MARK: - AppHandlerLinkOpener implementation
    
    func openLink(link: Link) -> Result<Void, AppHandlerError> {
        guard
            let filePath = link.file
        else {
            os_log("No file name in the link.", type: .error)
            return .failure(.noFilePathInLink)
        }
        
        guard
            let projectsDir = UserDefaults.standard.string(forKey: projectsDirKey)
        else {
            os_log("No projects dir set", type: .error)
            return .failure(.projectsDirNotSet)
        }
        
        let repo = link.repo
        let fullFilePath = "\(projectsDir)/\(repo)/\(filePath)"
        
        // Try different versions of Sublime Text
        let tryInFollowingOrder = ["Sublime Text", "Sublime Text 2"]
        
        for sublimeFlavour in tryInFollowingOrder {
            if NSWorkspace.shared.fullPath(forApplication: sublimeFlavour) != nil {
                let output = runner.run(
                    command: "/usr/bin/open",
                    with: [
                        "-a",
                        sublimeFlavour,
                        fullFilePath
                    ],
                    in: "/"
                )
                
                switch output {
                    case .none:
                        os_log("No output from Sublime Text execution command.", type: .error)
                        return .failure(.cantExecuteOpenerApp(message: "No output from Sublime Text"))
                    case .some(""):
                        // Command execution was successful.
                        return .success(())
                    case let .some(commandOutput):
                        print(commandOutput)
                        os_log("Sublime Text execution command produced error.", type: .error)
                        return .failure(.cantExecuteOpenerApp(message: commandOutput))
                }
            }
        }
        
        return .failure(.cantExecuteOpenerApp(message: "None of known Sublime Text flavours found."))
    }
    
}

