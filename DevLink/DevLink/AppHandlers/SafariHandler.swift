import Foundation
import os.log


final class SafariHandler: AppHandlerLinkSource {
    
    var bundleIdentifier: String { "com.apple.Safari" }
    
    init() {
        // Required for dynamic creation of instances basing on protocol
    }
    
    // MARK: - AppHandlerLinkSource implementation
    
    func extractLink() -> Result<Link, AppHandlerError> {
        let scriptSource = """
                tell application "Safari"
                    set theURL to URL of current tab of window 1
                end tell
                """
        
        let script = NSAppleScript(source: scriptSource)!
        var error: NSDictionary?
        let event = script.executeAndReturnError(&error)
        
        if
            let urlString = event.stringValue,
            let url = URL(string: urlString),
            let host = url.host,
            let hostingIntegration = GitHostingsRegistry().hostingIntegrations[host],
            let link = hostingIntegration.url2Link(url: url) {
            
            return .success(link)
        } else {
            if let error = error {
                print("Error: \(error)")
                os_log("Apple Script returned error", type: .error)
                return .failure(.noPermissionOrAppClosed(message: "Check in DevLink settings if you need to update permissions"))
            } else {
                os_log("Can't parse url or find hosting integration.", type: .error)
                return .failure(.noLinkReturnedByApp)
            }
        }
    }
    
}
