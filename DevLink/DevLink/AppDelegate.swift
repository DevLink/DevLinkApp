import Cocoa
import os.log


@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    let statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.squareLength)
    
    let appsRegistry: AppHandlersRegitstry = AppHandlersRegitstry()
    let hostingRegistry: GitHostingsRegistry = GitHostingsRegistry()
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Add status bar icon with menu
        statusItem.button?.title = "🖇"
        statusItem.menu = NSMenu()
        addMenuItems()
        
        // Register shortcut
        CarbonHotkeyHandler.register()
    }
    
    private func addMenuItems() {
        statusItem.menu?.addItem(withTitle: "Settings", action: #selector(showSettings), keyEquivalent: ",")
        statusItem.menu?.addItem(withTitle: "Quit", action: #selector(quit), keyEquivalent: "q")
    }
    
    @objc func showSettings(_ sender: NSMenuItem) {
        let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
        guard
            let vc = storyboard.instantiateController(withIdentifier: "ViewController") as? ViewController
        else {
            return
        }
        
        let popoverView = NSPopover()
        popoverView.contentViewController = vc
        popoverView.behavior = .transient
        popoverView.show(relativeTo: statusItem.button!.bounds, of: statusItem.button!, preferredEdge: .maxY)
    }
    
    @objc func quit(_ sender: NSMenuItem) {
        NSApp.terminate(self)
    }
    
    // MARK: - Handle custom URL scheme
    
    func application(_ application: NSApplication, open urls: [URL]) {
        // devlink:{action}?{parameters}
        
        for url in urls {
            guard
                let questionMarkIndex = url.absoluteString.firstIndex(of: "?")
            else {
                os_log("Can't parse callback URL", type: .error)
                continue
            }
            
            let actionName = url.absoluteString[..<questionMarkIndex]
            switch String(actionName) {
                case "devlink:localFile":
                    let fileIndex = url.absoluteString.index(after: questionMarkIndex)
                    let fileName = url.absoluteString[fileIndex...]
                    openFileOnWeb(filePath: String(fileName))
                case "devlink:gitHostingURL":
                    os_log("Not implemented yet.", type: .info)
                    break
                default:
                    os_log("Undefined action in callback URL", type: .error)
            }
        }
    }
    
    func openFileOnWeb(filePath: String) {
        switch GitWorktreeCommands().getHostingLink(for: filePath) {
            case let .success(link) :
                let hostingIntegration = hostingRegistry.hostingIntegrations[link.host]
                if let linkURL = hostingIntegration?.link2URL(link: link) {
                    NSWorkspace.shared.open(linkURL)
                } else {
                    os_log("Can't convert link to URL.", type: .error)
                }
            case let .failure(error):
                print(error)
                os_log("Can't open file on web.", type: .error)
        }
    }
    
}

