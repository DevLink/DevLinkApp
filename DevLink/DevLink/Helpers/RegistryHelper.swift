import Foundation


/// Invokes a given closure with a buffer containing all metaclasses known to the Obj-C runtime.
/// The buffer is only valid for the duration of the closure call.
func withAllClasses<R>(
    _ body: (UnsafeBufferPointer<AnyClass>) throws -> R
) rethrows -> R {
    
    var count: UInt32 = 0
    let classListPtr = objc_copyClassList(&count)
    
    defer {
        free(UnsafeMutableRawPointer(classListPtr))
    }
    
    let classListBuffer = UnsafeBufferPointer(
        start: classListPtr, count: Int(count)
    )
    
    return try body(classListBuffer)
}
