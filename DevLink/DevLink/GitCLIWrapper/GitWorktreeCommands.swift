import Foundation
import os.log


class GitWorktreeCommands {
    
    private let gitCommander: GitCommandRunner
    
    init(gitCommander: GitCommandRunner = DefaultGitCommandRunner()) {
        self.gitCommander = gitCommander
    }
    
    /// Get `Link` with git hosting information from which file was clonned.
    ///
    /// - Parameter filePath: Full file path
    /// - Returns: `Link` or error
    func getHostingLink(for filePath: String) -> Result<Link, AppHandlerError> {
        var pathComponents = filePath.split(separator: "/")
        let fileName = pathComponents.popLast()
        let fileDir = "/" + pathComponents.joined(separator: "/")
        
        guard
            let repoFileName = fileName
        else {
            os_log("Can't parse file name returned from Xcode link extraction string.", type: .error)
            return .failure(.cantParseFilePath)
        }
        
        guard
            let repoRoot = gitCommander.getRepoRoot(file: filePath)
        else {
            os_log("Can't parse file path or find git repo for this file", type: .error)
            return .failure(.notAGitRepo)
        }
        
        let remoteURL: URL
        switch gitCommander.getRepoRemote(file: filePath) {
            case let .failure(error):
                return .failure(error)
            case let .success(remote):
                remoteURL = remote
        }
        
        let remoteRepoPathComponents = remoteURL.path.split(separator: "/")
        
        guard
            let user = remoteRepoPathComponents.first,
            let repo = remoteRepoPathComponents.last?.replacingOccurrences(of: ".git", with: "")
        else {
            os_log("Can't parse `user/repo` from remote URL path", type: .error)
            return .failure(.noRemoteSetForRepo)
        }
        
        guard
            let remoteHost = remoteURL.host
        else {
            os_log("Can't parse host in remote URL.", type: .error)
            return .failure(.noRemoteSetForRepo)
        }
        
        let filePathOrNameInRepo = fileDir.replacingOccurrences(of: repoRoot, with: "")
        let filePathInRepo = filePathOrNameInRepo.replacingOccurrences(of: "/", with: "", options: .anchored)
        
        return .success(Link(host: remoteHost,
                             user: String(user),
                             repo: repo,
                             ref: "master",
                             file: filePathInRepo.isEmpty ? String(repoFileName) : filePathInRepo + "/" + repoFileName,
                             line: nil)
        )
    }
    
}
