import XCTest
@testable import DevLink


class GithubIntegrationTest: XCTestCase {
    
    func testTestURL2Link() {
        let integration = GithubIntegration()
        let githubURL = URL(string: "https://github.com/USER/REPO/blob/REF/FILE.EXT")!
        let expectedLink = Link(
            host: "github.com",
            user: "USER",
            repo: "REPO",
            ref: "REF",
            file: "FILE.EXT",
            line: nil)
        
        let convertedLink = integration.url2Link(url: githubURL)
        XCTAssertEqual(expectedLink, convertedLink)
    }
    
}
