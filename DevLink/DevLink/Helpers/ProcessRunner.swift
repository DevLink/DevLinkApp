import Foundation
import os.log


protocol ProcessRunner {
    func run(command: String, with args: [String], in dir: String) -> String?
}

class DefaultProcessRunner: ProcessRunner {
    
    /// Run a CLI tool and capture output.
    ///
    /// - Parameters:
    ///   - command: CLI app to run (with full path!)
    ///   - args: arguments to pass to the app
    ///   - dir: directory in which app should be executed
    /// - Returns: CLI app output or nil if execution failed.
    func run(command: String, with args: [String], in dir: String) -> String? {
        let process = Process()
        let outputPipe = Pipe()
        process.currentDirectoryPath = dir
        process.launchPath = command
        process.arguments = args
        process.standardOutput = outputPipe
        do {
            try process.run()
            let commandOutput = outputPipe.fileHandleForReading.readDataToEndOfFile()
            return String(data: commandOutput, encoding: .utf8)
        } catch {
            print("Running command line tool failed: ", error)
            os_log("Running command line tool failed with error.", type: .error)
            return nil
        }
    }
    
}
