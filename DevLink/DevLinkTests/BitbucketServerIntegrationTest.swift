import XCTest
@testable import DevLink


class BitbucketServerIntegrationTest: XCTestCase {
    
    func testFileURL2Link() {
        let integration = BitbucketServerIntegration()
        let hostingURL = URL(string: "https://stash.goodcompany.com/projects/USER/repos/REPO/browse/FILE.EXT")!
        let expectedLink = Link(
            host: "stash.goodcompany.com",
            user: "USER",
            repo: "REPO",
            ref: "",
            file: "FILE.EXT",
            line: nil)
        
        let convertedLink = integration.url2Link(url: hostingURL)
        XCTAssertEqual(expectedLink, convertedLink)
    }
    
    func testChangeURL2Link() {
        let integration = BitbucketServerIntegration()
        let hostingURL = URL(string: "https://stash.goodcompany.com/projects/USER/repos/REPO/pull-requests/PR/diff#FILE.EXT")!
        let expectedLink = Link(
            host: "stash.goodcompany.com",
            user: "USER",
            repo: "REPO",
            ref: "",
            file: "FILE.EXT",
            line: nil)
        
        let convertedLink = integration.url2Link(url: hostingURL)
        XCTAssertEqual(expectedLink, convertedLink)
    }
    
    func testChangeWithCommentURL2Link() {
        let integration = BitbucketServerIntegration()
        let hostingURL = URL(string: "https://stash.goodcompany.com/projects/USER/repos/REPO/pull-requests/PR/diff?commentId=COMMENT_ID#FILE.EXT?f=SOME_ID")!
        let expectedLink = Link(
            host: "stash.goodcompany.com",
            user: "USER",
            repo: "REPO",
            ref: "",
            file: "FILE.EXT",
            line: nil)
        
        let convertedLink = integration.url2Link(url: hostingURL)
        XCTAssertEqual(expectedLink, convertedLink)
    }
    
    func testCommitURL2Link() {
        let integration = BitbucketServerIntegration()
        let hostingURL = URL(string: "https://stash.goodcompany.com/projects/USER/repos/REPO/commits/REF#FILE.EXT")!
        let expectedLink = Link(
            host: "stash.goodcompany.com",
            user: "USER",
            repo: "REPO",
            ref: "", // TODO: Add commit parsing
            file: "FILE.EXT",
            line: nil)
        
        let convertedLink = integration.url2Link(url: hostingURL)
        XCTAssertEqual(expectedLink, convertedLink)
    }
    
}
