import XCTest
@testable import DevLink


class DefaultGitCommanderTest: XCTestCase {
    
    let mockRunner = MockProcessRunner()
    var gitCommander: DefaultGitCommandRunner? = nil
    
    override func setUp() {
        gitCommander = DefaultGitCommandRunner(runner: mockRunner)
    }
    
    override func tearDown() {
        mockRunner.output = nil
    }
    
    // MARK: - Repository root
    
    func testValidRepoRoot() {
        mockRunner.output = "/Users/Bob/Documents/Code/HelloWorld"
        let repoRoot = gitCommander?.getRepoRoot(file: "/Users/Bob/Documents/Code/HelloWorld/HelloWorld.js")
        
        XCTAssertEqual(repoRoot, "/Users/Bob/Documents/Code/HelloWorld",
                       "GitCommander should return repository root for a file in a valid git working copy.")
    }
    
    func testNotARepo() {
        mockRunner.output = "fatal: not a git repository (or any of the parent directories): .git"
        let repoRoot = gitCommander?.getRepoRoot(file: "/Users/Bob/Documents/Code/HelloWorld/HelloWorld.js")
        
        XCTAssertNil(repoRoot,
                     "Repository root shoud be `nil` if file is not in a git working copy.")
    }
    
    func testTestNoGitInstalled() {
        mockRunner.output = "zsh: command not found: git"
        let repoRoot = gitCommander?.getRepoRoot(file: "/Users/Bob/Documents/Code/HelloWorld/HelloWorld.js")
        
        XCTAssertNil(repoRoot,
                     "Repository root shoud be `nil` if git is not installed")
    }
    
    // MARK: - Remote for a local repository

    func testValidHTTPSRemote() throws {
        mockRunner.output = """
            origin\thttps://github.com/Bob/HelloWorld.git (fetch)
            origin\thttps://github.com/Bob/HelloWorld.git (push)
            """
        let remote = gitCommander?.getRepoRemote(file: "/Users/Bob/Documents/Code/HelloWorld/HelloWorld.js")
        
        let expectedURL = URL(string: "https://github.com/Bob/HelloWorld.git")!
        XCTAssertEqual(.success(expectedURL), remote)
    }
    
    func testValidSSHRemote() throws {
        mockRunner.output = """
            origin\tssh://git@stash.goodcompany.com:8995/Bob/HelloWorld.git (fetch)
            origin\tssh://git@stash.goodcompany.com:8995/Bob/HelloWorld.git (push)
            """
        let remote = gitCommander?.getRepoRemote(file: "/Users/Bob/Documents/Code/HelloWorld/HelloWorld.js")
        
        let expectedURL = URL(string: "ssh://git@stash.goodcompany.com:8995/Bob/HelloWorld.git")!
        XCTAssertEqual(.success(expectedURL), remote)
    }
    
}

class MockProcessRunner: ProcessRunner {
    var output: String? = nil
    
    func run(command: String, with args: [String], in dir: String) -> String? {
        return output
    }
}
