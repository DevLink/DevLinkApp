//
// This file is needed to add Objective-C runtime to the app.
// Objective-C runtime is used to dynamically find
// implementations of handlers during runtime.
//

@import Foundation;
