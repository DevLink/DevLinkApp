import Foundation
import os.log


protocol GitCommandRunner {
    func getRepoRoot(file: String) -> String?
    func getRepoRemote(file: String) -> Result<URL, AppHandlerError>
}

class DefaultGitCommandRunner: GitCommandRunner {
    
    private var runner: ProcessRunner
    
    init(runner: ProcessRunner = DefaultProcessRunner()) {
        self.runner = runner
    }
    
    func getRepoRoot(file: String) -> String? {
        let fileDir = NSString(string: file).deletingLastPathComponent
        
        let repoPath = runner.run(command: "/usr/bin/git", with: ["rev-parse", "--show-toplevel"], in: fileDir)
        guard
            let repoRoot = repoPath?.replacingOccurrences(of: "\n", with: ""),
            !repoRoot.contains("not a git repository"),
            !repoRoot.contains("command not found")
        else {
            os_log("Can't parse file path or find git repo for this file", type: .error)
            return nil
        }
        return repoRoot
    }
    
    func getRepoRemote(file: String) -> Result<URL, AppHandlerError> {
        let fileDir = NSString(string: file).deletingLastPathComponent
        
        let gitRemotes = runner.run(command: "/usr/bin/git", with: ["remote", "-v"], in: fileDir)
        guard let remotesString = gitRemotes else {
            print("`git remote` call failed.")
            os_log("`git remote` call failed.", type: .error)
            return .failure(.noRemoteSetForRepo)
        }
        
        let scanner = Scanner(string: remotesString)
        let prefix = scanner.scanUpToString("\t")
        let remoteURLString = scanner.scanUpToString(" ")
        
        #if DEBUG
        assert(prefix == "origin", "Unexpected prefix. Check if `git remove -v` command changed output format.")
        #endif
        
        guard
            let remote = remoteURLString,
            let remoteURL = URL(string: remote as String)
        else {
            print("Can't parse `git remote` output.")
            os_log("Can't parse `git remote` output.", type: .error)
            return .failure(.noRemoteSetForRepo)
        }
        
        return .success(remoteURL)
    }
}

