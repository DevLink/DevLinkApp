import Foundation


class GitHostingsRegistry {
    
    private (set) var hostingIntegrations: [String: GitHostingIntegrationProtocol] = [:]
    
    init() {
        // List of classes implementing GitHostingIntegrationProtocol
        let hostingIntegrationTypes = withAllClasses { $0.compactMap{ $0 as? GitHostingIntegrationProtocol.Type} }
        
        // Create instance of each class and add to the dictionary
        hostingIntegrations = hostingIntegrationTypes.reduce(into: [:]) { (result, handlerType) in
            let handler = handlerType.init()
            result[handler.host] = handler
        }
    }
    
}
