import XCTest
@testable import DevLink


class BitbucketCloudIntegrationTest: XCTestCase {
    
    func testFileURL2Link() {
        let integration = BitbucketCloudIntegration()
        let hostingURL = URL(string: "https://bitbucket.org/USER/REPO/src/REF/FILE.EXT")!
        let expectedLink = Link(
            host: "bitbucket.org",
            user: "USER",
            repo: "REPO",
            ref: "REF",
            file: "FILE.EXT",
            line: nil)
        
        let convertedLink = integration.url2Link(url: hostingURL)
        XCTAssertEqual(expectedLink, convertedLink)
    }
    
    func testChangeURL2Link() {
        let integration = BitbucketCloudIntegration()
        let hostingURL = URL(string: "https://bitbucket.org/USER/REPO/commits/REF#chg-FILE.EXT")!
        let expectedLink = Link(
            host: "bitbucket.org",
            user: "USER",
            repo: "REPO",
            ref: "REF",
            file: "FILE.EXT",
            line: nil)
        
        let convertedLink = integration.url2Link(url: hostingURL)
        XCTAssertEqual(expectedLink, convertedLink)
    }
    
    func testCommitURL2Link() {
        let integration = BitbucketCloudIntegration()
        let hostingURL = URL(string: "https://bitbucket.org/USER/REPO/commits/REF/FILE.EXT")!
        let expectedLink = Link(
            host: "bitbucket.org",
            user: "USER",
            repo: "REPO",
            ref: "REF",
            file: "FILE.EXT",
            line: nil)
        
        let convertedLink = integration.url2Link(url: hostingURL)
        XCTAssertEqual(expectedLink, convertedLink)
    }
    
}
