import Foundation


/// Protocol for integrations with different code
protocol GitHostingIntegrationProtocol {
    var host: String { get }
    
    init()
    func url2Link(url: URL) -> Link?
    func link2URL(link: Link) -> URL?
}
