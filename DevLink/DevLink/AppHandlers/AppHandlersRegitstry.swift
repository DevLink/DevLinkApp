import Foundation


final class AppHandlersRegitstry {
    
    var sourceHandlers: [String: AppHandlerLinkSource] = [:]
    var openerHandlers: [String: AppHandlerLinkOpener] = [:]
    
    init() {
        let sourceHandlerTypes = withAllClasses { $0.compactMap{ $0 as? AppHandlerLinkSource.Type} }
        
        sourceHandlers = sourceHandlerTypes.reduce(into: [:]) { (result, handlerType) in
            let handler = handlerType.init()
            result[handler.bundleIdentifier] = handler
        }
        
        let openHandlerTypes = withAllClasses { $0.compactMap{ $0 as? AppHandlerLinkOpener.Type} }
        openerHandlers = openHandlerTypes.reduce(into: [:]) { (result, handlerType) in
            let handler = handlerType.init()
            result[handler.bundleIdentifier] = handler
        }
    }
    
}
