import XCTest
@testable import DevLink


class GitWorktreeCommandsTest: XCTestCase {
    
    var mockGitCommander = MockGitCommander()
    var gwc: GitWorktreeCommands? = nil
    
    override func setUp() {
        gwc = GitWorktreeCommands(gitCommander: mockGitCommander)
    }
    
    override func tearDown() {
        mockGitCommander.root = nil
        mockGitCommander.remote = ""
    }
    
    func testGetLinkForFileInRootDir() {
        mockGitCommander.root = "/Users/Alice/Documents/Projects/HelloWorld"
        mockGitCommander.remote = "https://github.com/Alice/HelloWorld.git"
        
        let hostingLink = gwc?.getHostingLink(for: "/Users/Alice/Documents/Projects/HelloWorld/HelloWorld.c")
        
        XCTAssertNotNil(hostingLink,
                        "GitWorktreeCommands should return link for valid input.")
        
        switch hostingLink! {
            case let .success(link):
                XCTAssertEqual(link.file, "HelloWorld.c",
                               "GitWorktreeCommands should return file without path for a file in repo root.")
            case let .failure(error) :
                NSLog(error.localizedDescription)
                XCTFail("GitWorktreeCommands shouldn't fail for valid input.")
        }
    }
    
    func testGetLinkForFileInSubDir() {
        mockGitCommander.root = "/Users/Alice/Documents/Projects/HelloWorld/"
        mockGitCommander.remote = "https://github.com/Alice/HelloWorld.git"
        
        let hostingLink = gwc?.getHostingLink(for: "/Users/Alice/Documents/Projects/HelloWorld/Factory/AbstractFactory.c")
        
        XCTAssertNotNil(hostingLink, "GitWorktreeCommands should return link for valid input.")
        
        switch hostingLink! {
            case let .success(link):
                XCTAssertEqual(link.file, "Factory/AbstractFactory.c",
                               "GitWorktreeCommands should return file with relative path for a file in repo sub dir.")
            case let .failure(error) :
                NSLog(error.localizedDescription)
                XCTFail("GitWorktreeCommands shouldn't fail for valid input.")
        }
    }
    
    // TODO: Test a remote without protocol e.g. git@codeberg.org:DevLink/DevLinkApp.git
}

class MockGitCommander: GitCommandRunner {
    
    public var root: String? = nil
    public var remote: String = ""

    func getRepoRoot(file: String) -> String? {
        return root
    }
    
    func getRepoRemote(file: String) -> Result<URL, AppHandlerError> {
        let url = URL(string: remote)
        return url != nil ? .success(url!) : .failure(AppHandlerError.noRemoteSetForRepo)
    }
}

