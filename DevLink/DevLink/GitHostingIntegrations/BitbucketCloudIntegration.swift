import Foundation
import os.log


// Path formats:
// - /{user}/{repo}/src/{ref}/{file}
// - /{user}/{repo}/commits/{ref}#chg-{file}
// - /{user}/{repo}/commits/{ref}/{file}

final class BitbucketCloudIntegration: GitHostingIntegrationProtocol {
    
    var host: String { "bitbucket.org" }
    
    init() {
        // Required for dynamic creation of instances basing on protocol
    }
    
    func url2Link(url: URL) -> Link? {
        if url.absoluteString.contains("commits") {
            return parseCommitURL(url)
        } else {
            return parseRegularURL(url)
        }
    }
    
    func link2URL(link: Link) -> URL? {
        var urlComponents: [String] = [link.host, link.user, link.repo, "src", link.ref]
        
        if let file = link.file,
           let encodedFileName = file.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            urlComponents.append(encodedFileName)
        }
        
        let urlString = "https://" + urlComponents.joined(separator: "/")
        return URL(string: urlString)
    }
    
    // MARK: - Helpers
    
    private func parseRegularURL(_ url: URL) -> Link? {
        let pathComponents = url.path.split(separator: "/")
        
        let fileName = pathComponents.count >= 5 ? pathComponents.dropFirst(4).joined(separator: "/") : nil
        let encodedFileName = fileName?.removingPercentEncoding
        
        let link: Link = Link(host: host,
                              user: String(pathComponents[0]),
                              repo: String(pathComponents[1]),
                              ref: String(pathComponents[3]),
                              file: encodedFileName,
                              line: nil)
        
        return link
    }
    
    private func parseCommitURL(_ url: URL) -> Link? {
        let filteredURLString = url.absoluteString.replacingOccurrences(of: "#chg-", with: "/")
        guard
            let filteredURL = URL(string: filteredURLString)
        else {
            os_log("Can't parse bitbucket commit url", type: .error)
            return nil
        }
        
        let pathComponents = filteredURL.path.split(separator: "/")
        let fileName = pathComponents.count >= 5 ? pathComponents.dropFirst(4).joined(separator: "/") : nil
        let encodedFileName = fileName?.removingPercentEncoding
        
        let link: Link = Link(host: host,
                              user: String(pathComponents[0]),
                              repo: String(pathComponents[1]),
                              ref: String(pathComponents[3]),
                              file: encodedFileName,
                              line: nil)
        
        return link
    }
    
}
